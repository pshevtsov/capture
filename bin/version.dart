#!/usr/bin/env dart

import 'dart:io';
import 'dart:convert';
import 'package:args/args.dart';

const USAGE = 'usage: version [major|minor|build] manifest.json';

void usage() {
  print(USAGE);
  exit(1);
}

void version(String command, String manifest) {
  final file = new File(manifest);
  String src = file.readAsStringSync();
  var data = JSON.decode(src);
  List version = data['version'].split('.');
  version = version.map((n) => int.parse(n)).toList();

  switch (command) {
    case 'major':
      version[0]++;
      version[1] = 0;
      version[2] = 0;
      break;
    case 'minor':
      version[1]++;
      version[2] = 0;
      break;
    case 'build':
      version[2]++;
      break;
  }

  if (command != 'version') {
    data['version'] = version.join('.');
    var encoder = const JsonEncoder.withIndent('  ');
    src = encoder.convert(data);
    file.writeAsString(src);
  }

  print(version.join('.'));
}

void main(List<String> arguments) {
  final cmd = new ArgParser();
  final parser = new ArgParser()
      ..addCommand('major', cmd)
      ..addCommand('minor', cmd)
      ..addCommand('build', cmd);
  String command;
  String manifest;

  try {
    ArgResults results = parser.parse(arguments);
    command = (results.command != null) ? results.command.name : 'version';
    manifest = (results.command != null) ? results.command.rest[0] : results.rest[0];
  } catch (_) {
    usage();
  }
  version(command, manifest);
}
