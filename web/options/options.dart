import 'dart:async';
import 'dart:html';
import 'package:chrome/chrome_ext.dart' as chrome;

List keys = ['pages_url', 'submit_url', 'filter_tags', 'filter_attrs'];

void showModal(MouseEvent e) {
  LinkElement target = e.target;
  String modal_id = target.dataset['modal-id'];
  DivElement modal = document.getElementById(modal_id).clone(true);

  e.preventDefault();

  modal.attributes.remove('style');

  modal.querySelectorAll('button, .close-button').onClick.listen((_) {
    modal.classes.add('transparent');
    new Timer(new Duration(seconds: 1), () => modal.remove());
  });

  document.body.append(modal);
}

void saveData(_) {
  chrome.storage.sync.get(null).then((Map items) {
    String selectors = keys.map((key) => '#$key').join(',');

    document.querySelectorAll(selectors).forEach((InputElement input) =>
        items[input.id] = input.value);
    chrome.storage.sync.set(items).then((_) {
      window.close();
    });
  });
}

void main() {
  document.querySelectorAll('a.modal_trigger').onClick.listen(showModal);

  chrome.storage.sync.get(keys).then((Map items) {
    items.forEach((k, v) {
      InputElement input = document.getElementById(k);
      input.value = v;
    });
  });

  document.getElementById('save').onClick.listen(saveData);
}
