var Capture = (function () {

  // Properties ----------------------------------------------------------------

  /**
   * User options keys
   * @type {Array.<string>}
   */
  var _optionsKeys = ['filter_tags', 'filter_attrs'];

  /**
   * Options
   * @type {Object}
   */
  var _options = {};

  /**
   * Click counter
   * @type {number}
   */
  var _clickCounter = 0;

  /**
   * Capture end points
   * @type {Array.<Node>}
   */
  var _captureEndPoints = [];

  /**
   * Capture end points sorted by order in document
   * @type {Array.<Node>}
   */
  var _captureEndPointsSorted = [];

  /**
   * Capture fragment
   * @type {DocumentFragment}
   */
  var _captureFragment = document.createDocumentFragment();

  /**
   * Document Tree walker
   * @type {TreeWalker}
   */
  var _docTreeWalker = document.createTreeWalker(
    document.body,
    NodeFilter.SHOW_ELEMENT,
    {
      acceptNode: function (node) {
        return NodeFilter.FILTER_ACCEPT;
      }
    },
    false
  );

  /**
   * Fragment Tree Walker
   * @type {TreeWalker}
   */
  var _fragTreeWalker = document.createTreeWalker(
    _captureFragment,
    NodeFilter.SHOW_ELEMENT,
    {
      acceptNode: function (node) {
        return NodeFilter.FILTER_ACCEPT;
      }
    },
    false
  );


  // Constants -----------------------------------------------------------------

  /**
   * Maximum end points
   * @const
   */
  var MAX_END_POINTS = 2;

  /**
   * Auxillary CSS class names
   * @const
   */
  var AUX_CSS_CLASS_NAMES = {
    mouseover: 'capture--highlight',
    click: 'capture--selected'
  };


  // Methods -------------------------------------------------------------------

  /**
   * Resets Document Tree Walker
   */
  function _resetDocTreeWalker () {
    _docTreeWalker.currentNode = _docTreeWalker.root;
  }

  /**
   * Resets Fragment Tree Walker
   */
  function _resetFragTreeWalker () {
    _fragTreeWalker.currentNode = _fragTreeWalker.root;
  }

  /**
   * Sorts end point nodes
   */
  function _sortEndPoints () {
    if (_captureEndPoints && _captureEndPoints.length === MAX_END_POINTS) {
      _captureEndPointsSorted = [];
      _resetDocTreeWalker();

      while (_docTreeWalker.nextNode()) {
        if (_captureEndPoints.indexOf(_docTreeWalker.currentNode) !== -1) {
          _captureEndPointsSorted.push(_docTreeWalker.currentNode);
        }
      }

    } else {
      throw new Error('_captureEndPoints in empty or has incorrect length');
    }
  }

  /**
   * Performs the capture
   */
  function _capture () {
    var container = document.createElement('div');
    container.appendChild(_getCaptureFragment());
    container.normalize();

    chrome.runtime.sendMessage({capture: container.innerHTML});

    container = null;
  }

  /**
   * Gets the start point node
   * @return {Node}
   */
  function _getCaptureStartNode () {
    if (!_captureEndPointsSorted.length) {
      _sortEndPoints();
    }

    return _captureEndPointsSorted[0];
  }

  /**
   * Gets the end point node
   * @return {Node}
   */
  function _getCaptureEndNode () {
    if (!_captureEndPointsSorted.length) {
      _sortEndPoints();
    }

    return _captureEndPointsSorted[_captureEndPointsSorted.length - 1];
  }

  /**
   * Gets the capture fragment
   * @return {DocumentFragment}
   */
  function _getCaptureFragment () {
    if (_captureFragment.hasChildNodes()) {
      return _captureFragment;
    }

    var startNode = _getCaptureStartNode();
    var endNode = _getCaptureEndNode();

    if (startNode === endNode) {
      _captureFragment.appendChild(startNode.cloneNode(true));

      return _captureFragment;
    }

    var append = false;

    _resetDocTreeWalker();

    while (_docTreeWalker.nextNode()) {
      var hasStart = _containsEqualNode(
        _docTreeWalker.currentNode,
        startNode
      );
      var hasEnd = _containsEqualNode(
        _docTreeWalker.currentNode,
        endNode
      );
      var contains = _containsEqualNode(
        _captureFragment,
        _docTreeWalker.currentNode
      );

      if (hasStart) {
        append = true;
      } else if (hasEnd) {
        append = false;
      }

      if (!contains && (hasStart || hasEnd || append)) {
        _captureFragment.appendChild(
          _docTreeWalker.currentNode.cloneNode(true)
        );
      }
    }

    _filterCaptureFragment();

    return _captureFragment;
  }

  /**
   * Filter capture fragment
   */
  function _filterCaptureFragment () {
    var startNode = _getCaptureStartNode();
    var endNode = _getCaptureEndNode();
    var remove = true;
    var removeNodes = [];

    var inList = function(check, list) {
      list = list.split(' ');
      var l = list.length;
      for (var i = 0; i < l; i++) {
        if (check.toUpperCase() === list[i].toUpperCase()) {
          return true;
        }
      }
      return false;
    };

    var removeAttr = function(node, attrs) {
      attrs = attrs.split(' ');
      var l = attrs.length;
      for (var i = 0; i < l; i++) {
        node.removeAttribute(attrs[i]);
      }
    };

    _getCaptureFragment();
    _resetFragTreeWalker();

    // Remove redundant nodes
    while (_fragTreeWalker.nextNode()) {
      var hasStart = _containsEqualNode(
        _fragTreeWalker.currentNode,
        startNode
      );
      var hasEnd = _containsEqualNode(
        _fragTreeWalker.currentNode,
        endNode
      );

      if (_fragTreeWalker.currentNode.isEqualNode(startNode)) {
        remove = false;
      } else if (_fragTreeWalker.currentNode.isEqualNode(endNode)) {
        remove = true;
      }

      if (hasStart || hasEnd) {
        continue;
      }

      if (remove) {
        removeNodes.push(_fragTreeWalker.currentNode);
      }

      if (inList(_fragTreeWalker.currentNode.nodeName, _options.filter_tags)) {
        removeNodes.push(_fragTreeWalker.currentNode);
      }

      removeAttr(_fragTreeWalker.currentNode, _options.filter_attrs);
    }

    removeNodes.forEach(function (node) {
      if (node.parentNode) {
        node.parentNode.removeChild(node);
      }
    });
  }

  /**
   * Check if parent contains equal node
   * @param {Node} parent
   * @param {Node} node
   * @return {Boolean}
   */
  function _containsEqualNode (parent, node) {
    if (parent.isEqualNode(node)) {
      return true;
    }

    var treeWalker = document.createTreeWalker(
      parent,
      NodeFilter.SHOW_ELEMENT,
      {
        acceptNode: function (node) {
          return NodeFilter.FILTER_ACCEPT;
        }
      },
      false
    );

    while (treeWalker.nextNode()) {
      if (treeWalker.currentNode.isEqualNode(node)) {
        return true;
      }
    }

    return false;
  }


  // Event handlers ------------------------------------------------------------

  function _onClick (e) {
    if (e.shiftKey === false) {
      return;
    }

    e.preventDefault();

    var selection = window.getSelection();
    selection.removeAllRanges();

    var className = AUX_CSS_CLASS_NAMES[e.type];

    _captureEndPoints.push(e.target);
    e.target.classList.add(className);

    _clickCounter++;

    if (_clickCounter === MAX_END_POINTS) {
      Object.keys(AUX_CSS_CLASS_NAMES).forEach(function (k) {
        var className = AUX_CSS_CLASS_NAMES[k];
        var cssClassName = '.' + className;
        var elements = document.querySelectorAll(cssClassName);

        Array.prototype.forEach.call(elements, function (el) {
          el.classList.remove(className);
          if (el.classList.length === 0) {
            el.removeAttribute('class');
          }
        });
      });

      _capture();

      document.removeEventListener('click', _onClick);
      document.removeEventListener('mouseover', _onMouseOver);
    }
  }

  function _onMouseOver (e) {
    var className = AUX_CSS_CLASS_NAMES[e.type];
    var cssClassName = '.' + className;
    var elements = document.querySelectorAll(cssClassName);

    Array.prototype.forEach.call(elements, function (el) {
      el.classList.remove(className);
      if (el.classList.length === 0) {
        el.removeAttribute('class');
      }
    });

    if (e.shiftKey) {
      e.target.classList.add(className);
    }
  }


  // Init ----------------------------------------------------------------------

  function init () {
    chrome.storage.sync.get(_optionsKeys, function(options) {
      _options = options;
      document.addEventListener('click', _onClick);
      document.addEventListener('mouseover', _onMouseOver);
    });
  }


  // Public methods ------------------------------------------------------------

  return {
    init: init
  };

})();

Capture.init();
