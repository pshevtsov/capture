library background;

import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:html';
import 'package:chrome/chrome_ext.dart' as chrome;

class Background {
  int tabId;
  Queue<Map<String,String>> queue;

  Background() {
    chrome.browserAction.onClicked.listen(onBrowserActionClicked);
    chrome.runtime.onMessage.listen(onMessage);
    chrome.tabs.onRemoved.listen(onTabClose);
    chrome.tabs.onUpdated.listen(onTabUpdate);
  }

  /**
   * Create new tab
   */
  Future<chrome.Tab> createTab(String url) {
    var createProperties = new chrome.TabsCreateParams(url: url);

    return chrome.tabs.create(createProperties);
  }

  /**
   * Highlight existing tab
   */
  void highlightTab([int id]) {
    id = (id == null) ? tabId : id;

    chrome.tabs.get(id).then((tab) {
      if (tab.active == false) {
        var highlightInfo = new chrome.TabsHighlightParams(tabs: tab.index);

        chrome.tabs.highlight(highlightInfo);
      }
    });
  }

  /**
   * Get pages URL from options
   */
  Future<String> getUrl() {
    String key = 'pages_url';
    String cacheParam = new DateTime.now().millisecondsSinceEpoch.toString();

    return chrome.storage.sync.get(key).then((Map items) {
      if (items[key] != null) {
        var uri = Uri.parse(items[key]);
        Map<String, String> queryParams = new Map();

        uri.queryParameters.forEach((k, v) => queryParams[k] = v);
        queryParams[cacheParam] = '';

        uri = new Uri(scheme: uri.scheme, userInfo: uri.userInfo, host:
            uri.host, port: uri.port, path: uri.path, queryParameters: queryParams,
            fragment: uri.fragment);

        return uri.toString();
      }

      throw ("Pages JSON URL is empty");
    });
  }

  /**
   * Handle Ajax request error
   */
  Function handleError([String message]) {
    if (message == null) {
      message = 'Unknown Error';
    }
    return () {
      var options = new chrome.NotificationOptions(
          type: chrome.TemplateType.BASIC,
          iconUrl: chrome.runtime.getURL('/icon.png'),
          title: 'Screen Capture Error',
          message: message
          );
      chrome.notifications.create('error-message', options);
    };
  }

  /**
   * Get list of pages from JSON and store it into queue
   */
  Future<Queue<String>> getPagesQueue() {
    return getUrl().then((url) {
      return HttpRequest.getString(url).then((result) {
        return new Future.value(new Queue.from(JSON.decode(result)));
      }).catchError(handleError('Error getting pages JSON'));
    }).catchError(handleError('Error getting pages URL from options'));
  }

  /**
   * Check if URL exists
   */
  static Future<bool> checkUrl(String url) {
    return HttpRequest.request(url, method: 'HEAD').then((_) =>
        new Future.value(true)).catchError((_) => new Future.value(false));
  }

  /**
   * Process pages queue
   */
  Future<String> processQueue() {
    if (queue.isEmpty) {
      return new Future.error("Queue is empty");
    }
    Map<String,String> item = queue.removeFirst();
    String url = item['url'];

    return Background.checkUrl(url).then((exists) {
      if (exists) {
        if (item.containsKey('message') && item['message'].isNotEmpty) {
          var options = new chrome.NotificationOptions(
            type: chrome.TemplateType.BASIC,
            iconUrl: chrome.runtime.getURL('/icon.png'),
            title: url,
            message: item['message']
          );
          chrome.notifications.create('url-message-${url}', options);
        }
        return new Future.value(url);
      }
      return processQueue();
    });
  }

  /**
   * Handles Browser action click event
   */
  void onBrowserActionClicked(_) {
    if (tabId == null) {
      getPagesQueue().then((q) {
        queue = q;
        processQueue().then(createTab).then((tab) {
          tabId = tab.id;
        });
      });
    } else {
      highlightTab(tabId);
    }
  }

  /**
   * Update Tab content
   */
  void updateTab(String url) {
    var updateProperties = new chrome.TabsUpdateParams(url: url);
    chrome.tabs.update(updateProperties, tabId);
  }

  /**
   * Handle receiving message from content script
   */
  void onMessage(chrome.OnMessageEvent e) {
    if (e.message.hasProperty('capture')) {
      var data = {
        "url": e.sender.url,
        "capture": e.message['capture']
      };

      sendData(data).then((_) {
        processQueue().then(updateTab).catchError((_) {
          var options = new chrome.NotificationOptions(
            type: chrome.TemplateType.BASIC,
            iconUrl: chrome.runtime.getURL('/icon.png'),
            title: 'Screen Capture',
            message: 'All done!'
          );
          chrome.notifications.create('success-message', options);
          chrome.tabs.remove(tabId);
        });
      }).catchError((_) {
        var options = new chrome.NotificationOptions(
          type: chrome.TemplateType.BASIC,
          iconUrl: chrome.runtime.getURL('/icon.png'),
          title: 'Screen Capture Error',
          message: 'Request failed!'
        );
        chrome.notifications.create('error-message', options);
      });
    }
  }

  /**
   * Send data
   */
  sendData(data) {
    var key = 'submit_url';

    return chrome.storage.sync.get(key).then((Map items) {
      if (items[key] != null) {
        var url = items[key];

        return HttpRequest.postFormData(url, data);
      }
    });
  }

  /**
   * Handle closing tab
   */
  void onTabClose(chrome.TabsOnRemovedEvent e) {
    if (e.tabId == tabId) {
      tabId = null;
    }
  }

  /**
   * Handle updating tab
   */
  void onTabUpdate(chrome.OnUpdatedEvent e) {
    if (e.changeInfo['status'] == 'complete' && e.tabId == tabId) {
      var details = new chrome.InjectDetails(file: 'content/content.css');

      chrome.tabs.insertCSS(details).then((_) {
        var details = new chrome.InjectDetails(file: 'content/content.js');

        chrome.tabs.executeScript(details);
      });
    }
  }
}

main() {
  new Background();
}
