# capture - screen capture Chrome extension

BINDIR = bin
BUILDDIR = build
MODE = release
PKGDIR = packages
SRCDIR = web

all: packages build
	@echo Version: $(shell make version)
	@echo Build mode: ${MODE}
	@pub build --mode ${MODE}
	@rm -f *.crx
	@${BINDIR}/crxmake.sh ${BUILDDIR}/web capture.pem
	@mv web.crx capture-$(shell make version)-${MODE}.crx
	@echo capture-$(shell make version)-${MODE}.crx

debug: MODE = debug
debug: all

packages: pubspec.yaml
	@pub get

clean:
	@echo Cleaning
	@rm -rf ${BUILDDIR} ${PKGDIR} *.crx
	@find -name packages -type l | xargs rm -rf

major minor build: packages
	@${BINDIR}/version.dart $@ ${SRCDIR}/manifest.json > /dev/null

version:
	@${BINDIR}/version.dart ${SRCDIR}/manifest.json

.PHONY: all build clean debug major minor version
